import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import * as yup from "yup";
import IsiTable from "./component/isiTable";
import ModalDetail from "./component/Modals/modalDetail";
import ModalAddData from "./component/Modals/modalAddData";
import ModalEditData from "./component/Modals/modalEdit";
import ModalDeleteData from "./component/Modals/modalDeleteData";
import { ALLDATA } from "./Helpers/DataAPI/Users";
import Pagination from 'react-bootstrap/Pagination';
function App() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  const [refreshData, setRefreshData] = useState(0);
  const [idClicked, setIdClicked] = useState(null);

  const [openedModalAdd, setOpenedModalAdd] = useState(false);
  const [openedModalDetail, setOpenedModalDetail] = useState(false);
  const [openedModalEdit, setOpenedModalEdit] = useState(false);
  const [openedModalDelete, setOpenedModalDelete] = useState(false);
 
//  pagination

  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(5);
  const [searchData, setsearchData] = useState("");
  const [totalPaginate, setTotalPaginate] = useState(1)
  
  // end pagination
  

  let itemsNumber = [];
  for (let number = 1; number <= totalPaginate; number++) {
    itemsNumber.push(
      <Pagination.Item key={number} active={number === page} onClick={e=>setPage(e.target.text)}>
        {number}
      </Pagination.Item>,
    );
  }

  useEffect(() => {
    ALLDATA(page, perPage, searchData)
      .then(function (response) {
        // handle success
        // console.log(response.data);
        setTotalPaginate( Math.ceil(response?.data?.total_data / response?.data?.per_page))
        setIsLoaded(true);
        setItems(response.data.data);

        // console.log(items);
      })
      .catch(function (error) {
        // handle error
        // console.log(error);

        setIsLoaded(true);
        setError(error);
      });
  }, [refreshData, searchData, page, perPage]);

  // console.log(isLoaded);
  return (
    <div className="wrapper">
      <div className="container mt-2">
        <div className="row align-items-start">
          <div className="col">
            <h4>List Employee</h4>
          </div>
          <div className="col">
            <form className="d-flex">
              <input
                className="form-control me-2"
                type="search"
                placeholder="Search"
                aria-label="Search"
                onChange={(e) => setsearchData(e?.target?.value)}
                value={searchData}
              />
            </form>
          </div>
          <div className="col">
            <button
              type="button"
              className="btn btn-primary"
              onClick={() => setOpenedModalAdd(true)}
            >
              ADD
            </button>
          </div>
        </div>
        <ModalAddData
          refreshData={refreshData}
          setRefreshData={setRefreshData}
          open={openedModalAdd}
          setOpen={setOpenedModalAdd}
        />

        <table className="table">
          <thead>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">Name</th>
              <th scope="col">Email</th>
              <th scope="col">Mobile</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {items.map((data, key) => {
              return (
                // isi table
                <IsiTable
                  key={data._id}
                  name={data.name}
                  email={data.email}
                  mobile={data.mobile}
                  id={data._id}
                  setIdClicked={setIdClicked}
                  setOpen={setOpenedModalDetail}
                  setOpenedModalEdit={setOpenedModalEdit}
                  setOpenedModalDelete={setOpenedModalDelete}
                ></IsiTable>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="container">
        <p style={{ marginRight: 20 }}>Show</p>
        <ul className="pagination">
          <select
            className=""
            aria-label=".form-select-lg example"
            style={{ marginRight: 20 }}
        onChange={e=>setPerPage(e.target.value)}
        value={perPage}
          >
            <option value="5">5</option>
            <option value="10">10</option>
            <option value="20">20</option>

          </select>
         
        </ul>

        <Pagination>{itemsNumber}</Pagination>
      </div>


      {/* openedModalDetail, setOpenedModalDetail */}
      <ModalDetail
        id={idClicked}
        open={openedModalDetail}
        setOpen={setOpenedModalDetail}
      />

      <ModalEditData
        id={idClicked}
        refreshData={refreshData}
        setRefreshData={setRefreshData}
        open={openedModalEdit}
        setOpen={setOpenedModalEdit}
      />

      <ModalDeleteData
        id={idClicked}
        refreshData={refreshData}
        setRefreshData={setRefreshData}
        open={openedModalDelete}
        setOpen={setOpenedModalDelete}
      />
    </div>
  );
}

export default App;
