import axios from 'axios';

const apiClientJm = axios.create({
  // baseURL: 'https://it-development.jasamarga.co.id:8010',
  baseURL: `http://51.159.67.219:9002/v1/`,
  headers: {
    // eslint-disable-next-line
    // Authorization: 'Bearer ' + localStorage.getItem('jwt_access_token'),
    'Content-Type': 'application/json',
  },
  crossDomain: true,
});

apiClientJm.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('jwt_access_token');

    // eslint-disable-next-line
    config.headers['Authorization'] = 'Bearer ' + token;

    // config.headers['Content-Type'] = 'application/json';
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

export default apiClientJm;
