import API from "../baseAPI";

export const ALLDATA = (page,perPage,search ) =>{
    return API.get(`user?page=${page}&perPage=${perPage}&search=${search}`)
}

export const DETAILUSER = (id) =>{
    return API.get(`user/${id}`)
} 

export const DELETEUSER = (id) =>{
    return API.delete(`user/${id}`)
}



