import React, { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import * as yup from "yup";

const Modals = ({ refreshData, setRefreshData,open,setOpen }) => {
  const handleClose = () => setOpen(false);
  const handleShow = () => setOpen(true);

  const [upData, setupData] = useState({
    name: "",
    email: "",
    mobile: "",
    birthdate: "",
    address: "",
  });

  const handleChange = (e) => {
    e.persist();
    setupData((upData) => ({
      ...upData,
      [e.target.name]: e.target.value,
    }));
    // console.log(e.target.value)
  };
  const handleSubmit = (e) => {
    // console.log(upData)
    let data = yup.object().shape({
      name: yup.string().required(),
      email: yup.string().email().required(),
      mobile: yup.number().required().positive().integer(),
      birthdate: yup.string().required(),
      address: yup.string().required(),
    });

    data
      .validate({
        name: upData.name,
        email: upData.email,
        mobile: upData.mobile,
        birthdate: upData.birthdate,
        address: upData.address,
      })
      .then(function (valid) {
        // console.log(valid)
        if (valid) {
          setupData({
            name: upData.name,
            email: upData.email,
            mobile: upData.mobile,
            birthdate: upData.birthdate,
            address: upData.address,
          });

          axios.post(`http://51.159.67.219:9002/v1/user`, {
            name: valid.name,
            email: valid.email,
            mobile: valid.mobile,
            birthdate: valid.birthdate,
            address: valid.address,
          });
        }

        setRefreshData(refreshData + 1);
        setOpen(false)
        // alert("Wait ...");
      })
      .catch(function (err) {
        // setConditional(err.errors);
        // console.log(err)
        alert(err.errors)
      });
  };

  return (
    <>
      <Modal show={open} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row g-3 align-items-center mb-3">
            <div className="col-2">
              <label htmlFor="inputPassword6" className="col-form-label">
                Name
              </label>
            </div>
            <div className="col-auto ms-5">
              <input
                type="text"
                className="form-control"
                name="name"
                id="name"
                placeholder="Name"
                value={upData.name || ""}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="row g-3 align-items-center mb-3">
            <div className="col-2">
              <label htmlFor="inputPassword6" className="col-form-label">
                Email
              </label>
            </div>
            <div className="col-auto ms-5">
              <input
                type="email"
                className="form-control"
                name="email"
                id="email"
                placeholder="Email"
                value={upData.email || ""}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="row g-3 align-items-center mb-3">
            <div className="col-2">
              <label htmlFor="inputPassword6" className="col-form-label">
                Mobile
              </label>
            </div>
            <div className="col-auto ms-5">
              <input
                type="number"
                className="form-control"
                name="mobile"
                id="mobile"
                placeholder="mobile"
                value={upData.mobile || ""}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="row g-3 align-items-center mb-3">
            <div className="col-2">
              <label htmlFor="inputPassword6" className="col-form-label">
                Birthdate
              </label>
            </div>
            <div className="col-auto ms-5">
              <input
                type="date"
                className="form-control"
                name="birthdate"
                id="birthdate"
                placeholder="BirthDate"
                value={upData.birthdate || ""}
                onChange={handleChange}
              />
            </div>
          </div>

          <div className="row g-3 align-items-center mb-3">
            <div className="col-2">
              <label htmlFor="inputPassword6" className="col-form-label">
                Address
              </label>
            </div>
            <div className="col-auto ms-5">
              <input
                type="text"
                className="form-control"
                name="address"
                id="address"
                placeholder="Your Address"
                value={upData.address || ""}
                onChange={handleChange}
              />
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleSubmit}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Modals;
