import React, { useEffect, useState } from "react";
import axios from "axios";
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { DETAILUSER } from "../../Helpers/DataAPI/Users";

const Modals = ({open,setOpen ,id}) =>{
 const [valuesOfModals, setValuesOfModals] = useState({})

    const handleClose = () => setOpen(false);
    const handleShow = () => setOpen(true);

    useEffect(() => {
      DETAILUSER(id).then(data=>setValuesOfModals(data.data.data)).catch(err=>console.log(err))


    }, [open])
    
    return (
  
      <>

  
      <Modal show={open} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>

        <div className="row align-items-start">
                    <div className="col-3" id="objectid">ID  </div>
                    <div className="col" id="address">: {valuesOfModals?._id}</div>
                  </div>
        <div className="row align-items-start mt-3">
                    <div className="col-3" id="objectid">Name</div>
                    <div className="col" id="address">: {valuesOfModals?.name}</div>
                  </div>
        <div className="row align-items-start mt-3">
                    <div className="col-3" id="objectid">Email </div>
                    <div className="col" id="address">: {valuesOfModals?.email}</div>
                  </div>
        <div className="row align-items-start mt-3">
                    <div className="col-3" id="objectid">Mobile </div>
                    <div className="col" id="address">: {valuesOfModals?.mobile}</div>
                  </div>
        <div className="row align-items-start mt-3">
                    <div className="col-3" id="objectid">BirthDate </div>
                    <div className="col" id="address">: {valuesOfModals?.birthdate}</div>
                  </div>
        <div className="row align-items-start mt-3">
                    <div className="col-3" id="objectid">Adress : </div>
                    <div className="col" id="address">: {valuesOfModals?.address}</div>
                  </div>


        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          {/* <Button variant="primary" onClick={handleClose}>
            Save Changes
          </Button> */}
        </Modal.Footer>
      </Modal>
    </>
    )
  }

  export default Modals