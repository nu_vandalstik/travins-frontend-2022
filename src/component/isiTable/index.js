import React, { useEffect, useState } from "react";
import axios from "axios";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

function Index({ name, email, mobile, id,setIdClicked, setOpen,setOpenedModalEdit,setOpenedModalDelete }) {
  return (
    <>
      <tr>
        <td >{id || "Empty"}</td>
        <td>{name || "Empty"}</td>
        <td>{email || "Empty"}</td>
        <td>{mobile || "Empty"}</td>
        <td>
          <button
            type="button"
            className="btn btn-primary ms-1"
            onClick={()=>(setIdClicked(id),setOpen(true))}
            // data-bs-toggle="modal"
            // data-bs-target="#detail"
            // onClick={}
          >
            Detail
          </button>

          <button
            type="button"
            className="btn btn-warning ms-1"
            onClick={()=>(setIdClicked(id),setOpenedModalEdit(true))}
            // data-bs-toggle="modal"
            // data-bs-target="#edit"
          >
            Edit
          </button>

          <button
            type="button"
            className="btn btn-danger ms-1"
            onClick={()=>(setIdClicked(id),setOpenedModalDelete(true))}
            // data-bs-toggle="modal"
            // data-bs-target="#delete"
          >
            Delete
          </button>
        </td>
      </tr>

      {/* <Modals/> */}
    </>
  );
}

export default Index;
